# official default python base image
FROM python:3

# create working directory
WORKDIR /src

# add host files to container file system
ADD . /src

# install dependencies
RUN pip install -r requirements.txt

# expose internal container port which the application runs on
EXPOSE 4840

# executed after the build process, starts the server 
CMD ["python", "server-minimal.py"]
